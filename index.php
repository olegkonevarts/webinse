<!doctype html>

<html lang="en">
    <head>
        <!-- `Meta` -->
        <meta charset="utf-8">
        <meta name="viewport"
              content="width=device-width">
        <meta http-equiv="x-ua-compatible"
              content="ie=edge">
        
        <!-- Browser tab info -->
        <link rel="icon"
              href="favicon.ico">
        <title>Webinse — Entrance test for internship</title>
        
        <!-- `Meta`. `SEO` -->
        <meta name="description"
              content="">
        <meta name="keywords"
              content="">
        
        <!-- `CSS` -->
        <link rel="stylesheet"
              href="styles/css/main.css">
        
        <!-- `JS` -->
        <script src="scripts/js/library/functions/superglobal/selectors.js"></script>
        
        <script src="scripts/js/library/classes/superglobal/Replace.js"></script>
        <script src="scripts/js/library/classes/superglobal/Random.js"></script>
        <script src="scripts/js/library/classes/superglobal/Number.js"></script>
        
        <script src="scripts/js/library/classes/global/ContextMenu.js"></script>
        <script src="scripts/js/library/classes/global/Paste.js"></script>
        <script src="scripts/js/library/classes/global/KeyEvent.js"></script>
        <script src="scripts/js/library/classes/global/Ajax.js"></script>
        
        <script src="scripts/js/main.js"></script>
    </head>
    
    <body></body>
</html>

window.onload = function () {
    let firstNameId, secondNameId;
    let emailId;
    
    let createButton, deleteButtons;
    let tableContent;
    
    class Init {
        static variables() {
            firstNameId = $('#first-name');
            secondNameId = $('#second-name');
            
            emailId = $('#e-mail');
            
            createButton = $('.create');
            deleteButtons = $$('.delete');
            
            tableContent = $$('table td');
        }
    }
    
    class Person {
        static checkFirstName(firstName) {
            let maxLength = 1023;
            
            if (firstName.length > maxLength) {
                alert(
                    'Error!\n\n' +
                    
                    'Too long first name.\n\n' +
                    
                    `Max length is ${maxLength} characters.`
                );
            } else {
                let firstNameWithoutExtraSpaces = Replace.extraSpaces(firstName);
                
                if (firstNameWithoutExtraSpaces === '') {
                    alert(
                        'Error!\n\n' +
                        
                        'First name cannot be empty.'
                    );
                } else {
                    return {
                        checked: true,
                        value: firstNameWithoutExtraSpaces
                    };
                }
            }
            
            return {
                checked: false
            };
        }
        
        static checkSecondName(secondName) {
            let maxLength = 1023;
            
            if (secondName.length > maxLength) {
                alert(
                    'Error!\n\n' +
                    
                    'Too long second name.\n\n' +
                    
                    `Max length is ${maxLength} characters.`
                );
            } else {
                let secondNameWithoutExtraSpaces = Replace.extraSpaces(secondName);
                
                if (secondNameWithoutExtraSpaces === '') {
                    alert(
                        'Error!\n\n' +
                        
                        'Second name cannot be empty.'
                    );
                } else {
                    return {
                        checked: true,
                        value: secondNameWithoutExtraSpaces
                    };
                }
            }
            
            return {
                checked: false
            };
        }
        
        static checkEmail(email) {
            let maxLength = 255;
            
            if (email.length > maxLength) {
                alert(
                    'Error!\n\n' +
                    
                    'Too long e-mail.\n\n' +
                    
                    `Max length is ${maxLength} characters.`
                );
            } else {
                let emailWithoutSpaces = Replace.removeSpaces(email);
                
                let expression =
                    /^([a-z0-9]+|(([a-z0-9]+([.\-]))+[a-z0-9]+))@([a-z0-9]+\.)+[a-z]{2,}$/gi;
                
                if (! emailWithoutSpaces.match(expression)) {
                    alert(
                        'Error!\n\n' +
                        
                        'Incorrect e-mail.'
                    );
                } else {
                    return {
                        checked: true,
                        value: emailWithoutSpaces
                    };
                }
            }
            
            return {
                checked: false
            };
        }
        
        static setToLocalStorage() {
            localStorage.setItem(
                'first-name',
                $('#first-name').innerText
            );
            
            localStorage.setItem(
                'second-name',
                $('#second-name').innerText
            );
            
            localStorage.setItem(
                'e-mail',
                $('#e-mail').innerText
            );
        }
        
        static getFromLocalStorage() {
            firstNameId.innerText = localStorage.getItem('first-name');
            secondNameId.innerText = localStorage.getItem('second-name');
            
            emailId.innerText = localStorage.getItem('e-mail');
        }
        
        static removeFromLocalStorage() {
            localStorage.removeItem('first-name');
            localStorage.removeItem('second-name');
            
            localStorage.removeItem('e-mail');
        }
    }
    
    class Crud {
        static showContent(responseText) {
            function prepareTable(json) {
                let table = '';
                
                let tableOpen = '<table class="flex-row-center">';
                let tableClose = '</table>';
                
                let tableHeader = '';
                let tableInput = '';
                let tableContent = '';
                
                tableHeader += (
                    '<tr>' +
                    '    <th>First name</th>' +
                    '    <th>Second name</th>' +
                    '    ' +
                    '    <th>E-mail</th>' +
                    '    ' +
                    '    <th class="table-transparent"></th>' +
                    '</tr>'
                );
                
                tableInput += (
                    '<tr>' +
                    '    <td id="first-name"></td>' +
                    '    <td id="second-name"></td>' +
                    '    ' +
                    '    <td id="e-mail"></td>' +
                    '    ' +
                    '    <th class="table-transparent">' +
                    '        <button class="create">Add a person</button>' +
                    '    </th>' +
                    '</tr>'
                );
                
                json.reverse().forEach((row) => {
                    if (row['errorMessage'] !== undefined) {
                        if (row['errorMessage']) {
                            alert(
                                'Error!\n\n' +
                                
                                'This e-mail address is already in use.'
                            );
                        }
                    } else {
                        tableContent +=
                            '<tr>' +
                            `    <td>${row['first_name']}</td>` +
                            `    <td>${row['second_name']}</td>` +
                            '    ' +
                            `    <td>${row['e_mail']}</td>` +
                            '    ' +
                            '    <th class="table-transparent">' +
                            '        <button class="delete"' +
                            `                value="${row['id']}">` +
                            '            <text>Delete</text>' +
                            '        </button>' +
                            '    </th>' +
                            '</tr>';
                    }
                });
                
                table += (
                    tableOpen +
                    
                    tableHeader +
                    tableInput +
                    tableContent +
                    
                    tableClose
                );
                
                return table;
            }
            
            let json = JSON.parse(responseText);
            
            if (json) {
                $('body').innerHTML = prepareTable(json);
                
                Events.restart();
            }
        }
        
        static create(event) {
            event.preventDefault();
            
            let checkFirstNameResult = Person.checkFirstName(firstNameId.innerText);
            let checkSecondNameResult = Person.checkSecondName(secondNameId.innerText);
            
            let checkEmailResult = Person.checkEmail(emailId.innerText);
            
            if (
                checkFirstNameResult.checked &&
                checkSecondNameResult.checked &&
                
                checkEmailResult.checked
            ) {
                Ajax.post({
                        first_name: checkFirstNameResult.value,
                        second_name: checkSecondNameResult.value,
                        
                        e_mail: checkEmailResult.value
                    },
                    './scripts/php/crud/create.php',
                    Crud.showContent
                );
                
                Person.removeFromLocalStorage();
            }
        }
        
        static read() {
            Ajax.post(
                null,
                './scripts/php/crud/read.php',
                Crud.showContent
            );
        }
        
        static update() {
            function getTableRow(event) {
                let idValue = null;
                
                let elements = event.target.parentNode.childNodes;
                
                let rowValues = [];
                
                elements.forEach((element) => {
                    if (element.nodeName === 'TH') {
                        let children = element.childNodes;
                        
                        children.forEach((element) => {
                            if (element.nodeName === 'BUTTON') {
                                idValue = element.value;
                            }
                        });
                    } else if (element.nodeName === 'TD') {
                        rowValues.push(element.innerText);
                    }
                });
                
                /**
                 * Check
                 */
                let checkFirstNameResult = Person.checkFirstName(rowValues[0]);
                let checkSecondNameResult = Person.checkSecondName(rowValues[1]);
                
                let checkEmailResult = Person.checkEmail(rowValues[2]);
                
                if (
                    idValue > 0 &&
                    
                    checkFirstNameResult.checked &&
                    checkSecondNameResult.checked &&
                    
                    checkEmailResult.checked
                ) {
                    Ajax.post({
                            id: idValue,
                            
                            first_name: checkFirstNameResult.value,
                            second_name: checkSecondNameResult.value,
                            
                            e_mail: checkEmailResult.value
                        },
                        './scripts/php/crud/update.php',
                        null,
                        0
                    );
                    
                    console.log('All changes saved.');
                }
            }
            
            tableContent.forEach((value) => {
                if (value.id === '') {
                    value.removeEventListener('keyup', getTableRow, false);
                    value.addEventListener('keyup', getTableRow, false);
                }
                
                function _1() {
                    value.removeEventListener('mousedown', _2, false);
                    value.addEventListener('mousedown', _2, false);
                }
                
                function _2() {
                    if (value.className === 'active') {
                        return false;
                    }
                    
                    tableContent.forEach((value) => {
                        value.setAttribute('contenteditable', 'false');
                        value.classList.remove('active');
                    });
                    
                    value.setAttribute('contenteditable', 'true');
                    value.classList.add('active');
                }
                
                value.removeEventListener('mouseover', _1, false);
                value.addEventListener('mouseover', _1, false);
            });
            
            function _3(event) {
                if (! (event.target['nodeName'] === 'TD')) {
                    tableContent.forEach((value) => {
                        value.setAttribute('contenteditable', 'false');
                        value.classList.remove('active');
                    });
                }
            }
            
            removeEventListener('mousedown', _3, false);
            addEventListener('mousedown', _3, false);
        }
        
        static delete() {
            deleteButtons.forEach((value) => {
                function action(event) {
                    event.preventDefault();
                    
                    if (confirm(
                        'Warning!\n\n' +
                        
                        'Are you sure you want to permanently delete this note?'
                    )) {
                        let idValue = event.target.value;
                        
                        Ajax.post(
                            {id: idValue},
                            './scripts/php/crud/delete.php',
                            Crud.showContent
                        );
                    }
                }
                
                value.removeEventListener('click', action, false);
                value.addEventListener('click', action, false);
            });
        }
    }
    
    class Events {
        static remove() {
            firstNameId.removeEventListener('keyup', Person.setToLocalStorage, false);
            secondNameId.removeEventListener('keyup', Person.setToLocalStorage, false);
            
            emailId.removeEventListener('keyup', Person.setToLocalStorage, false);
            
            removeEventListener('click', Person.setToLocalStorage, false);
            
            removeEventListener('paste', Paste.fromClipboard, false);
            
            createButton.removeEventListener('click', Crud.create, false);
            
            removeEventListener('contextmenu', ContextMenu.off);
        }
        
        static add() {
            firstNameId.addEventListener('keyup', Person.setToLocalStorage, false);
            secondNameId.addEventListener('keyup', Person.setToLocalStorage, false);
            
            emailId.addEventListener('keyup', Person.setToLocalStorage, false);
            
            addEventListener('click', Person.setToLocalStorage, false);
            
            addEventListener('paste', Paste.fromClipboard, false);
            
            createButton.addEventListener('click', Crud.create, false);
            
            addEventListener('contextmenu', ContextMenu.off);
        }
        
        static restart() {
            Init.variables();
            
            this.remove();
            this.add();
            
            Person.getFromLocalStorage();
            
            Crud.update();
            Crud.delete();
        }
    }
    
    Crud.read();
};

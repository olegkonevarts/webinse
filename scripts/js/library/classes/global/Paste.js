class Paste {
    static fromClipboard(event) {
        event.preventDefault();
        
        const text = event.clipboardData.getData('text/plain');
        
        window.document.execCommand('insertText', false, text);
    }
}

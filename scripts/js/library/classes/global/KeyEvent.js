class KeyEvent {
    constructor() {
        self.isPreviousControl = false;
    }
    
    static isControl(event) {
        if (
            event.key === 'Control' ||
            event.ctrlKey === true
        ) {
            self.isPreviousControl = true;
            
            return true;
        } else {
            self.isPreviousControl = false;
            
            return false;
        }
    }
    
    static isControlAndIsPreviousControl(event) {
        if (
            (
                event.key !== 'v' &&
                event.key !== 'x' &&
                event.key !== 'z'
            ) &&
            self.isPreviousControl ||
            this.isControl(event)
        ) {
            setTimeout(() => {
                self.isPreviousControl = false;
            }, 100);
            
            return true;
        }
    }
}

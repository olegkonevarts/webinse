class Ajax {
    static post(requests, url, action, actionTimeout = 0) {
        function prepareRequest() {
            let fullRequest = '';
            
            if (typeof requests === 'object') {
                for (let request in requests) {
                    if (requests.hasOwnProperty(request)) {
                        if (request === 'id') {
                            fullRequest += `${request}=${requests[request]}&`;
                        } else {
                            fullRequest += `${request}=${encodeURIComponent(requests[request])}&`;
                        }
                    }
                }
                
                fullRequest = fullRequest.slice(0, -1);
            }
            
            fullRequest = checkRequest(fullRequest);
            
            return fullRequest;
        }
        
        function checkRequest(fullRequest) {
            if (fullRequest === '') {
                return null;
            } else {
                return fullRequest;
            }
        }
        
        function sendRequest() {
            xhr.open('POST', url, true);
            
            xhr.setRequestHeader(
                'Content-Type',
                'application/x-www-form-urlencoded'
            );
            
            xhr.send(prepareRequest());
        }
        
        function runAction() {
            xhr.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    if (this.responseText) {
                        if (typeof action === 'function') {
                            setTimeout(() => {
                                action(this.responseText);
                            }, actionTimeout);
                        }
                    }
                }
            };
        }
        
        function init() {
            sendRequest();
            
            runAction();
        }
        
        let xhr = new XMLHttpRequest();
        
        init();
    }
}

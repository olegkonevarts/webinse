class Random {
    static getIntegerExcluded(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
    
    static getIntegerIncluded(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}

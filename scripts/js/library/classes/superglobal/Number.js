class Number {
    static addPrefixZeros(valuesArray, number) {
        let start;
        let end;
        
        if (valuesArray.length > 0) {
            start = 1;
            end = valuesArray.length.toString().length;
            
            let numberLength = number.toString().length;
            
            if (numberLength < end) {
                let zeros = '';
                let length = end - numberLength;
                
                for (let i = start; i <= length; i++) {
                    zeros += '0';
                }
                
                number = zeros + number;
            }
            
            return number;
        } else {
            return false;
        }
    }
}

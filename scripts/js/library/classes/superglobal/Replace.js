class Replace {
    static extraSpaces(value) {
        value = value.replace(/\s+/gim, ' ');
        value = value.replace(/^\s+/gim, '');
        value = value.replace(/\s+$/gim, '');
        
        return value;
    }
    
    static removeSpaces(value) {
        value = value.replace(/\s+/gim, '');
        
        return value;
    }
}

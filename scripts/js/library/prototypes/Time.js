/**
 * Set | Time. Client
 */
function NowClientTime() {
    let date = new Date();
    
    /**
     * Sources
     */
    this.sources = {
        time: date.getTime(),
        
        offset: new TransformClientTime(date).elements.timezone.withGMT
    };
}

/**
 * Transform | Time. Client
 */
function TransformClientTime(time) {
    time = new Date(time);
    
    /**
     * Transformations
     */
    let transformations = {
        year: function () {
            return time.getFullYear().toString();
        },
        
        month: function () {
            let month = (time.getMonth() + 1).toString();
            
            if (month.length === 1) {
                month = `0${month}`;
            }
            
            return month;
        },
        
        day: function () {
            let day = time.getDate().toString();
            
            if (day.length === 1) {
                day = `0${day}`;
            }
            
            return day;
        },
        
        hours: function () {
            let hours = time.getHours().toString();
            
            if (hours.length === 1) {
                hours = `0${hours}`;
            }
            
            return hours;
        },
        minutes: function () {
            let minutes = time.getMinutes().toString();
            
            if (minutes.length === 1) {
                minutes = `0${minutes}`;
            }
            
            return minutes;
        },
        
        seconds: function () {
            let seconds = time.getSeconds().toString();
            
            if (seconds.length === 1) {
                seconds = `0${seconds}`;
            }
            
            return seconds;
        },
        
        milliseconds: function () {
            return (
                time.getTime().toString()
                .split('').reverse().join('')
                .substr(0, 3)
                .split('').reverse().join('')
            );
        },
        
        secondsSince1970: function () {
            return (
                time.getTime().toString()
                .split('').reverse().join('')
                .substr(3)
                .split('').reverse().join('')
            );
        },
        
        timezone: function () {
            let timezone = '±--:--';
            let match =
                time.toString()
                .match(/(GMT|UTC)((\+|-|)\d{2})(\d{2})/i);
            
            if (match !== null) {
                timezone = `${match[2]}:${match[4]}`;
            }
            
            return timezone;
        }
    };
    
    /**
     * Elements
     */
    this.elements = {
        year: transformations.year(),
        month: transformations.month(),
        day: transformations.day(),
        
        hours: transformations.hours(),
        minutes: transformations.minutes(),
        seconds: transformations.seconds(),
        
        milliseconds: transformations.milliseconds(),
        
        secondsSince1970: transformations.secondsSince1970(),
        
        timezone: {
            withoutGMT: transformations.timezone(),
            withGMT: `GMT${transformations.timezone()}`
        }
    };
    
    /**
     * Preparing | Extends
     */
    this.preparingFullTimeWithoutTimezone = function () {
        return (
            `${this.elements.year}.${this.elements.month}.${this.elements.day},` +
            `${this.elements.hours}:${this.elements.minutes}:${this.elements.seconds},` +
            `.${this.elements.milliseconds}`
        );
    };
    
    /**
     * Formed | Extends
     */
    this._extends = {
        secondsSince1970WithMilliseconds:
            this.elements.secondsSince1970 +
            `.${this.elements.milliseconds}`,
        
        fullTime: {
            withoutTimezone: this.preparingFullTimeWithoutTimezone(),
            
            withTimezone:
                this.preparingFullTimeWithoutTimezone() +
                ` (${this.elements.timezone.withGMT})`
        },
        
        hTMLDate:
            `${this.elements.year}-${this.elements.month}-${this.elements.day}`,
        
        _default:
            `${this.elements.year}.${this.elements.month}.${this.elements.day},` +
            `${this.elements.hours}:${this.elements.minutes}:${this.elements.seconds}`
    };
}

/**
 * Runtime
 */
function Runtime(startTime, finishTime) {
    this.startTime = new TransformClientTime(startTime.sources.time)
        ._extends.secondsSince1970WithMilliseconds;
    
    this.finishTime = new TransformClientTime(finishTime.sources.time)
        ._extends.secondsSince1970WithMilliseconds;
    
    /**
     * Calculations
     */
    this.runtime = (this.finishTime - this.startTime).toFixed(3);
    
    /**
     * Result
     */
    this.toString = function () {
        return (
            `Start time: ${this.startTime}\n` +
            `Finish time: ${this.finishTime}\n` +
            `Runtime: ${this.runtime}`
        );
    };
    
    /**
     * Log
     */
    console.group('Runtime:');
    console.log(this.toString());
    console.groupEnd();
}

function convertServerTimeToUserTime(serverTime, replacementField) {
    let matches = serverTime.match(
        /(\d{4})\.(\d{2})\.(\d{2}), (\d{2}):(\d{2}):(\d{2})/
    );
    
    let timestampServer = new Date(
        +matches[1], +matches[2] - 1, +matches[3],
        +matches[4], +matches[5], +matches[6]
    ).getTime();
    
    let timeOffsetUser =
        new Date(timestampServer).getTimezoneOffset() * 60 * 1000;
    
    let timestampUser = timestampServer - timeOffsetUser;
    
    replacementField.innerText =
        new TransformClientTime(timestampUser)._extends._default;
}

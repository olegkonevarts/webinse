<?php

class Database
{
    use TimeTrait;
    
    private $db;
    
    public function __construct()
    {
        $this->db = $this->connect();
    }
    
    private function connect()
    {
        $this->db = new mysqli(
            DB_HOST,
            DB_USERNAME,
            DB_PASSWORD,
            DB_NAME
        );
        
        if ($this->db->connect_errno) {
            exit($this->db->connect_error);
        } else {
            $this->db->set_charset(DB_CHARSET);
            
            return $this->db;
        }
    }
    
    private function escapeValue(&$value)
    {
        $value = $this->db->real_escape_string(
            htmlspecialchars($value)
        );
        
        $value = "'$value'";
        
        if ($value === "''") {
            $value = 'NULL';
        }
    }
    
    private function escapeTitle(&$name)
    {
        $name = preg_replace(
            '/`{2,}/',
            '`',
            '`' . htmlspecialchars($name) . '`'
        );
    }
    
    # Create
    
    public function createUserAndSchemaWithGrant(
        # ! Run from the `root` user without `DB_NAME`
        string $userName, string $userPassword,
        string $schemaName,
        string $hostName = 'localhost'
    )
    {
        /**
         * Escape
         */
        $this->escapeValue($userName);
        $this->escapeValue($hostName);
        
        $fullUserName = "$userName@$hostName";
        
        $this->escapeValue($userPassword);
        
        $this->escapeTitle($schemaName);
        
        /**
         * Query
         */
        $query =
            "
                CREATE USER IF NOT EXISTS $fullUserName
                    IDENTIFIED WITH 'mysql_native_password'
                    BY $userPassword;
                
                CREATE SCHEMA IF NOT EXISTS $schemaName
                    CHARACTER SET 'utf8mb4'
                    COLLATE 'utf8mb4_unicode_ci';
                
                GRANT ALL
                    ON $schemaName.*
                    TO $fullUserName;
            ";
        
        /**
         * Result
         */
        $result = $this->db->multi_query($query);
        
        return $result;
    }
    
    public function createSchemaWithGrant(
        # ! Run without `DB_NAME`, or `DB_NAME = ''`
        string $userName, string $schemaName,
        string $hostName = 'localhost'
    )
    {
        /**
         * Escape
         */
        $this->escapeValue($userName);
        $this->escapeValue($hostName);
        
        $fullUserName = "$userName@$hostName";
        
        $this->escapeTitle($schemaName);
        
        /**
         * Query
         */
        $query =
            "
                CREATE SCHEMA IF NOT EXISTS $schemaName
                    CHARACTER SET 'utf8mb4'
                    COLLATE 'utf8mb4_unicode_ci';
                
                GRANT ALL
                    ON $schemaName.*
                    TO $fullUserName;
            ";
        
        /**
         * Result
         */
        $result = $this->db->multi_query($query);
        
        return $result;
    }
    
    public function createTable( // TODO: FOREIGN KEY
        string $tableName, array $titlesAndParameters
    )
    {
        /**
         * Escape
         */
        $this->escapeTitle($tableName);
        
        foreach ($titlesAndParameters as $key => $value) {
            unset($titlesAndParameters[$key]);
            
            $this->escapeTitle($key);
            
            $titlesAndParameters[$key] = $value;
        }
        
        /**
         * Query
         */
        $query = "CREATE TABLE IF NOT EXISTS $tableName (";
        
        $length = count($titlesAndParameters);
        $i = 0;
        $comma = ', ';
        
        foreach ($titlesAndParameters as $key => $value) {
            $query .= $key;
            
            if ($i === $length - 1) {
                $comma = '';
            }
            
            $query .= ' '
                . mb_strtoupper(join(' ', $value))
                . $comma;
            
            $i++;
        }
        
        $query .= ') ENGINE = INNODB;';
        
        /**
         * Result
         */
        $result = $this->db->query($query);
        
        return $result;
    }
    
    public function insert(
        string $tableName, array $titlesAndValues
    )
    {
        /**
         * Escape
         */
        $this->escapeTitle($tableName);
        
        foreach ($titlesAndValues as $key => $value) {
            unset($titlesAndValues[$key]);
            
            $this->escapeTitle($key);
            $this->escapeValue($value);
            
            $titlesAndValues[$key] = $value;
        }
        
        /**
         * Query
         */
        $query = "INSERT INTO $tableName (";
        
        $length = count($titlesAndValues);
        $i = 0;
        $comma = ', ';
        
        foreach ($titlesAndValues as $key => $value) {
            if ($key === '`id`') {
                $titlesAndValues[$key] = 'NULL';
            }
            
            if ($i === $length - 1) {
                $comma = '';
            }
            
            $query .= "$key$comma";
            
            $i++;
        }
        
        $query .= ') VALUES (';
        
        $length = count($titlesAndValues);
        $i = 0;
        $comma = ', ';
        
        foreach ($titlesAndValues as $key => $value) {
            if ($i === $length - 1) {
                $comma = '';
            }
            
            $query .= "$value$comma";
            
            $i++;
        }
        
        $query .= ');';
        
        /**
         * Result
         */
        $result = $this->db->query($query);
        
        return $result;
    }
    
    # Read
    
    public function select(
        string $tableName, array $columnNames = ['*'],
        string $whereColumnName = '', string $whereSign = '=', string $whereValue = '',
        string $orderByColumnName = 'id', string $orderType = '',
        int $limit = 100
    )
    {
        /**
         * Escape
         */
        $this->escapeTitle($tableName);
        
        foreach ($columnNames as $key => $value) {
            $this->escapeTitle($value);
            $columnNames[$key] = $value;
        }
        
        $this->escapeTitle($whereColumnName);
        $this->escapeValue($whereValue);
        
        $this->escapeTitle($orderByColumnName);
        
        /**
         * Query
         */
        $query = 'SELECT ';
        
        $length = count($columnNames);
        $comma = ', ';
        
        for ($i = 0; $i < $length; $i++) {
            if ($i === $length - 1) {
                $comma = '';
            }
            
            $query .= "{$columnNames[$i]}$comma";
        }
        
        $query .= " FROM $tableName";
        
        $whereSigns = [
            '=', '!=', '<>', '<', '>', '<=', '>=',
        ];
        
        if (
            $whereColumnName !== 'NULL' &&
            in_array($whereSign, $whereSigns) &&
            $whereValue !== 'NULL'
        ) {
            $query .= " WHERE $whereColumnName $whereSign $whereValue";
        }
        
        if ($orderType !== 'DESC') {
            $orderType = '';
        } else {
            $orderType = ' DESC';
        }
        
        if ($orderByColumnName !== '`') {
            $query .= " ORDER BY $orderByColumnName$orderType";
        }
        
        if ($limit > 0) {
            $query .= " LIMIT $limit";
        }
        
        $query .= ';';
        
        /**
         * Result
         */
        $result = $this->db->query($query);
        
        /**
         * Check & Return
         */
        if ($result) {
            $data = [];
            
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            
            return $data;
        } else {
            return false;
        }
    }
    
    public function selectCount(
        string $tableName, string $columnName,
        string $whereColumnName = '', string $whereSign = '=', string $whereValue = ''
    )
    {
        /**
         * Escape
         */
        $this->escapeTitle($tableName);
        $this->escapeTitle($columnName);
        
        $this->escapeTitle($whereColumnName);
        $this->escapeValue($whereValue);
        
        /**
         * Query
         */
        $query = "SELECT COUNT($columnName) FROM $tableName";
        
        $whereSigns = [
            '=', '!=', '<>', '<', '>', '<=', '>=',
        ];
        
        if (
            $whereColumnName !== 'NULL' &&
            in_array($whereSign, $whereSigns) &&
            $whereValue !== 'NULL'
        ) {
            $query .= " WHERE $whereColumnName $whereSign $whereValue";
        }
        
        $query .= ';';
        
        /**
         * Result
         */
        $result = $this->db->query($query);
        
        /**
         * Check & Return
         */
        if ($result) {
            $data = [];
            
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            
            return $data;
        } else {
            return false;
        }
    }
    
    public function join()
    {
        // TODO
    }
    
    public function selectPublicationsTop10()
    {
        /**
         * Query
         */
        $query =
            "
                SELECT
                    publications.author_name_first,
                    publications.text,
                    # LEFT(publications.text, 100),
                    publications.time_server,
                    COUNT(publication_id) AS comments_count,
                    publications.id
                    FROM comments
                    
                    LEFT JOIN publications ON
                        comments.publication_id = publications.id
                    
                    GROUP BY publication_id
                    ORDER BY
                        comments_count DESC,
                        publications.id
                    
                    LIMIT 10;
            ";
        
        /**
         * Result
         */
        $result = $this->db->query($query);
        
        /**
         * Check & Return
         */
        if ($result) {
            $data = [];
            
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            
            return $data;
        } else {
            return false;
        }
    }
    
    # Update
    
    public function update(
        string $tableName,
        array $columnsAndValues,
        string $whereColumnName, string $whereValue
    )
    {
        /**
         * Escape
         */
        $this->escapeTitle($tableName);
        
        foreach ($columnsAndValues as $key => $value) {
            unset($columnsAndValues[$key]);
            
            $this->escapeTitle($key);
            $this->escapeValue($value);
            
            $columnsAndValues[$key] = $value;
        }
        
        $this->escapeTitle($whereColumnName);
        $this->escapeValue($whereValue);
        
        /**
         * Query
         */
        $query = "UPDATE $tableName ";
        $query .= 'SET ';
        
        $length = count($columnsAndValues);
        $i = 0;
        $comma = ', ';
        
        foreach ($columnsAndValues as $key => $value) {
            if ($key === '`id`') {
                $i++;
                
                continue;
            }
            
            if ($i === $length - 1) {
                $comma = '';
            }
            
            $query .= "$key = $value$comma";
            
            $i++;
        }
        
        $query .= " WHERE $whereColumnName = $whereValue;";
        
        /**
         * Result
         */
        $result = $this->db->query($query);
        
        return $result;
    }
    
    # Delete
    
    public function dropRow(
        string $tableName,
        string $columnName, string $columnValue
    )
    {
        /**
         * Escape
         */
        $this->escapeTitle($tableName);
        
        $this->escapeTitle($columnName);
        $this->escapeValue($columnValue);
        
        /**
         * Query
         */
        $query = "DELETE FROM $tableName WHERE $columnName = BINARY $columnValue;";
        
        /**
         * Result
         */
        $result = $this->db->query($query);
        
        return $result;
    }
    
    public function dropRows(
        string $tableName,
        string $columnName, array $columnValues
    )
    {
        // TODO
    }
    
    public function dropColumn(string $tableName, string $columnName)
    {
        /**
         * Escape
         */
        $this->escapeTitle($tableName);
        
        $this->escapeTitle($columnName);
        
        /**
         * Query
         */
        $query = "ALTER TABLE $tableName DROP COLUMN $columnName;";
        
        /**
         * Result
         */
        $result = $this->db->query($query);
        
        return $result;
    }
    
    public function dropColumns(string $tableName, array $columnNames)
    {
        // TODO
    }
    
    public function dropTable(string $tableName)
    {
        /**
         * Escape
         */
        $this->escapeTitle($tableName);
        
        /**
         * Query
         */
        $query = "DROP TABLE IF EXISTS $tableName;";
        
        /**
         * Result
         */
        $result = $this->db->query($query);
        
        return $result;
    }
    
    public function dropSchema(string $schemaName)
    {
        /**
         * Escape
         */
        $this->escapeTitle($schemaName);
        
        /**
         * Query
         */
        $query = "DROP SCHEMA IF EXISTS $schemaName;";
        
        /**
         * Result
         */
        $result = $this->db->query($query);
        
        return $result;
    }
    
    public function dropUser(
        string $userName,
        string $hostName = 'localhost'
    )
    {
        /**
         * Escape
         */
        $this->escapeValue($userName);
        $this->escapeValue($hostName);
        
        $fullUserName = "$userName@$hostName";
        
        /**
         * Query
         */
        $query = "DROP USER IF EXISTS $fullUserName;";
        
        /**
         * Result
         */
        $result = $this->db->query($query);
        
        return $result;
    }
}

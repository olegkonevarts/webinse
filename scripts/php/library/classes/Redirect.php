<?php

class Redirect
{
    public static function toMain()
    {
        header('Location: /');
        
        exit();
    }
    
    public static function toRead()
    {
        header('Location: /scripts/php/crud/read.php');
        
        exit();
    }
}

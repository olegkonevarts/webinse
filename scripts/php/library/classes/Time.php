<?php

class Time
{
    use TimeTrait;
    
    public function getUtc()
    {
        return $this->utc();
    }
    
    public function getByTimezone(string $timezone)
    {
        return $this->byTimezone($timezone);
    }
    
    public function getClientApi()
    {
        return $this->clientApi();
    }
}

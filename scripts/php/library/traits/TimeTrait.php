<?php

trait TimeTrait
{
    private $timeMicroRaw = null;
    private $seconds = null;
    private $microseconds = null;
    
    private $dateFormat = null;
    
    protected $timeMicro = null;
    
    private function initialize()
    {
        $this->setTimeMicroRaw();
        $this->setSeconds();
        $this->setMicroseconds();
        $this->setTimeMicro();
        
        # $this->dateFormat = "l, F — Y.m.d, H:i:s.{$this->microseconds} \(\G\M\TP\)";
        $this->dateFormat = "Y.m.d, H:i:s.{$this->microseconds} \(\G\M\TP\)";
    }
    
    private function setTimeMicroRaw()
    {
        $this->timeMicroRaw = microtime();
    }
    
    private function setSeconds()
    {
        $this->seconds = substr($this->timeMicroRaw, 11);
    }
    
    private function setMicroseconds()
    {
        $this->microseconds = substr($this->timeMicroRaw, 2, 6);
    }
    
    private function setTimeMicro()
    {
        $this->timeMicro = "{$this->seconds}.{$this->microseconds}";
    }
    
    protected function utc()
    {
        $this->initialize();
        date_default_timezone_set('UTC');
        
        return preg_replace(
            '/GMT\+00\:00/',
            'UTC',
            date($this->dateFormat, $this->timeMicro)
        );
    }
    
    protected function byTimezone(string $timezone)
    {
        $this->initialize();
        date_default_timezone_set($timezone);
        
        return date($this->dateFormat, $this->timeMicro);
    }
    
    protected function clientApi()
    {
        # $whois = shell_exec("whois {$_SERVER['REMOTE_ADDR']}");
        # var_dump($whois);
        
        $ip = $_SERVER['REMOTE_ADDR'];
        
        $ipInfo = file_get_contents('http://ip-api.com/json/' . $ip); // TODO: HTTP > HTTPS
        $ipInfo = json_decode($ipInfo);
        
        $timeUser = '—';
        
        // var_dump($ipInfo);
        
        if (isset($ipInfo->timezone)) {
            $timezone = $ipInfo->timezone;
            // var_dump($timezone);
            
            $timeUser = $this->byTimezone($timezone);
        }
        
        // var_dump($timeUser);
        
        return $timeUser;
    }
}

<?php

$ds = DIRECTORY_SEPARATOR;

require_once __DIR__ . "$ds.preparation.php";

/**
 * Check & Drop
 */
if (
    ! empty($_POST['id']) &&
    $_POST['id'] > 0
) {
    $db->dropRow('users', 'id', $_POST['id']);
}

require_once 'read.php';

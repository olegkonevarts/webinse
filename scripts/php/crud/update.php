<?php

$ds = DIRECTORY_SEPARATOR;

require_once __DIR__ . "$ds.preparation.php";

$errorMessage = false;

/**
 * Check & Insert
 */
if (
    $_POST['id'] > 0 &&
    (
        ! empty($_POST['first_name']) ||
        ! empty($_POST['second_name']) ||
        
        ! empty($_POST['e_mail'])
    )
) {
    $result = $db->update(
        'users',
        [
            'updated_time_utc' => $time->getUtc(),
            'updated_time_user' => $time->getClientApi(),
            
            'first_name' => $_POST['first_name'],
            'second_name' => $_POST['second_name'],
            
            'e_mail' => $_POST['e_mail'],
        ],
        'id', $_POST['id']
    );
    
    if (! $result) {
        $errorMessage = true;
    }
}

require_once 'read.php';

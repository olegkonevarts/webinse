<?php

$ds = DIRECTORY_SEPARATOR;

require_once __DIR__ . "$ds.preparation.php";

/**
 * Get | Data
 */
$data = $db->select(
    'users',
    [
        'id',
        
        'first_name',
        'second_name',
        
        'e_mail',
    ]
);

/**
 * Check
 */
if (! is_iterable($data)) {
    $data = [];
}

$data[] = ['errorMessage' => (@$errorMessage || false)];

echo json_encode($data);

<?php

$ds = DIRECTORY_SEPARATOR;

require_once __DIR__ . "$ds.preparation.php";

$errorMessage = false;

/**
 * Check & Insert
 */
if (
    ! empty($_POST['first_name']) &&
    ! empty($_POST['second_name']) &&
    
    ! empty($_POST['e_mail'])
) {
    $result = $db->insert(
        'users',
        [
            'id' => null,
            
            'created_time_utc' => $time->getUtc(),
            'created_time_user' => $time->getClientApi(),
            
            'first_name' => $_POST['first_name'],
            'second_name' => $_POST['second_name'],
            
            'e_mail' => $_POST['e_mail'],
        ]
    );
    
    if (! $result) {
        $errorMessage = true;
    }
}

require_once 'read.php';

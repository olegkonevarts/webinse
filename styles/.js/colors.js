/**
 * 2014. `MATERIAL DESIGN`. Color palettes
 * https://material.io/design/color/the-color-system.html#tools-for-picking-colors
 */
let colors = {
    red: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#ffebee',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#ffcdd2',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#ef9a9a',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#e57373',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#ef5350',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#f44336',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#e53935',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#d32f2f',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#c62828',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#b71c1c',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#ff8a80',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#ff5252',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#ff1744',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#d50000',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                }
            }
        }
    },
    
    pink: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#fce4ec',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#f8bbd0',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#f48fb1',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#f06292',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#ec407a',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#e91e63',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#d81b60',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#c2185b',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#ad1457',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#880e4f',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#ff80ab',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#ff4081',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#f50057',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#c51162',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                }
            }
        }
    },
    
    purple: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#f3e5f5',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#e1bee7',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#ce93d8',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#ba68c8',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#ab47bc',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#9c27b0',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#8e24aa',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#7b1fa2',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#6a1b9a',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#4a148c',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#ea80fc',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#e040fb',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#d500f9',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#aa00ff',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                }
            }
        },
        
        deep: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#ede7f6',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#d1c4e9',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#b39ddb',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#9575cd',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#7e57c2',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#673ab7',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#5e35b1',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#512da8',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#4527a0',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#311b92',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#b388ff',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#7c4dff',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#651fff',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#6200ea',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                }
            }
        }
    },
    
    indigo: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#e8eaf6',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#c5cae9',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#9fa8da',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#7986cb',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#5c6bc0',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#3f51b5',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#3949ab',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#303f9f',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#283593',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#1a237e',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#8c9eff',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#536dfe',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#3d5afe',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#304ffe',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                }
            }
        }
    },
    
    blue: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#e3f2fd',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#bbdefb',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#90caf9',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#64b5f6',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#42a5f5',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#2196f3',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#1e88e5',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#1976d2',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#1565c0',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#0d47a1',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#82b1ff',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#448aff',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#2979ff',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#2962ff',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                }
            }
        },
        
        light: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#e1f5fe',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#b3e5fc',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#81d4fa',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#4fc3f7',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#29b6f6',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#03a9f4',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#039be5',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#0288d1',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#0277bd',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#01579b',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#80d8ff',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#40c4ff',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#00b0ff',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#0091ea',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                }
            }
        }
    },
    
    cyan: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#e0f7fa',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#b2ebf2',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#80deea',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#4dd0e1',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#26c6da',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#00bcd4',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#00acc1',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#0097a7',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#00838f',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#006064',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#84ffff',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#18ffff',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#00e5ff',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#00b8d4',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    }
                }
            }
        }
    },
    
    teal: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#e0f2f1',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#b2dfdb',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#80cbc4',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#4db6ac',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#26a69a',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#009688',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#00897b',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#00796b',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#00695c',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#004d40',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#a7ffeb',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#64ffda',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#1de9b6',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#00bfa5',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    }
                }
            }
        }
    },
    
    green: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#e8f5e9',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#c8e6c9',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#a5d6a7',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#81c784',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#66bb6a',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#4caf50',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#43a047',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#388e3c',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#2e7d32',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#1b5e20',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#b9f6ca',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#69f0ae',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#00e676',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#00c853',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    }
                }
            }
        },
        
        light: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#f1f8e9',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#dcedc8',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#c5e1a5',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#aed581',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#9ccc65',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#8bc34a',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#7cb342',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#689f38',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#558b2f',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#33691e',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#ccff90',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#b2ff59',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#76ff03',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#64dd17',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    }
                }
            }
        }
    },
    
    lime: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#f9fbe7',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#f0f4c3',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#e6ee9c',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#dce775',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#d4e157',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#cddc39',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#c0ca33',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#afb42b',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#9e9d24',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#827717',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#f4ff81',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#eeff41',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#c6ff00',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#aeea00',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    }
                }
            }
        }
    },
    
    yellow: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#fffde7',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#fff9c4',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#fff59d',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#fff176',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#ffee58',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#ffeb3b',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#fdd835',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#fbc02d',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#f9a825',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#f57f17',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#ffff8d',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#ffff00',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#ffea00',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#ffd600',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    }
                }
            }
        }
    },
    
    amber: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#fff8e1',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#ffecb3',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#ffe082',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#ffd54f',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#ffca28',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#ffc107',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#ffb300',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#ffa000',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#ff8f00',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#ff6f00',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#ffe57f',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#ffd740',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#ffc400',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#ffab00',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    }
                }
            }
        }
    },
    
    orange: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#fff3e0',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#ffe0b2',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#ffcc80',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#ffb74d',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#ffa726',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#ff9800',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#fb8c00',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#f57c00',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#ef6c00',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#e65100',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#ffd180',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#ffab40',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#ff9100',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#ff6d00',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    }
                }
            }
        },
        
        deep: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#fbe9e7',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#ffccbc',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#ffab91',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#ff8a65',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#ff7043',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#ff5722',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#f4511e',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#e64a19',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#d84315',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#bf360c',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                },
                
                android: {
                    _100: {
                        hexadecimal: '#ff9e80',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#ff6e40',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#ff3d00',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#dd2c00',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                }
            }
        }
    },
    
    brown: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#efebe9',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#d7ccc8',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#bcaaa4',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#a1887f',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#8d6e63',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#795548',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#6d4c41',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#5d4037',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#4e342e',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#3e2723',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                }
            }
        }
    },
    
    grey: {
        normal: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#fafafa',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#f5f5f5',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#eeeeee',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#e0e0e0',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#bdbdbd',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#9e9e9e',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#757575',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#616161',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#424242',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#212121',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                }
            }
        },
        
        blue: {
            shades: {
                default: {
                    _50: {
                        hexadecimal: '#eceff1',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _100: {
                        hexadecimal: '#cfd8dc',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _200: {
                        hexadecimal: '#b0bec5',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _300: {
                        hexadecimal: '#90a4ae',
                        contrast: 'rgba(0, 0, 0, 0.87)'
                    },
                    _400: {
                        hexadecimal: '#78909c',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _500: {
                        hexadecimal: '#607d8b',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _600: {
                        hexadecimal: '#546e7a',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _700: {
                        hexadecimal: '#455a64',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _800: {
                        hexadecimal: '#37474f',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    },
                    _900: {
                        hexadecimal: '#263238',
                        contrast: 'rgba(255, 255, 255, 0.87)'
                    }
                }
            }
        }
    }
};

/**
 * Convert | To `SCSS`
 */
// let cycles = new Cycles();
// console.log(cycles.objectToString('colors'));

/**
 * Console. Test it
 */
// console.log(colors);
// console.log(colors.red.normal.shades.default._500.hexadecimal);

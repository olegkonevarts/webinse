## Webinse
### Entrance test for internship

---

#### How to use the `Database` class? `CRUD` examples
##### Create
```diff
+ Tested
```

```diff
@@ Create | User & Schema. With rights @@
@@ ! Run as `root` user @@
```
```php
$db = new Database();

// $db->dropUser('webinse');
// $db->dropSchema('users');

$db->createUserAndSchemaWithGrant(
    'webinse',
    
    /**
     * Generate | Password. Random:
     * https://konev.co/generate-a-random-password/
     */
    'NX@k@S/yCG*+$Sk{2K&,91Jq!9JCDj:|',
    
    'users'
);
```

---

```diff
+ Tested
```

```diff
@@ Create | Table @@
```
```php
$db = new Database();

$db->createTable(
    'users',
    [
        'id' => ['serial', 'primary key'],
        
        'created_time_utc' => ['varchar(63)', 'not null'],
        'created_time_user' => ['varchar(63)'],
        
        'updated_time_utc' => ['varchar(63)'],
        'updated_time_user' => ['varchar(63)'],
        
        'first_name' => ['varchar(1023)', 'not null'],
        'second_name' => ['varchar(1023)', 'not null'],
        
        'e_mail' => ['varchar(255)', 'not null', 'unique'],
    ]
);
```

---

```diff
+ Tested
```

```diff
@@ Insert | Data. To table @@
```
```php
$db = new Database();
$time = new Time();

$db->insert(
    'users',
    [
        'id' => null,
        
        'created_time_utc' => $time->getUtc(),
        'created_time_user' => $time->getClientApi(),
        
        'first_name' => 'Oleg',
        'second_name' => 'Konev',
        
        'e_mail' => 'oleg.konev.arts@gmail.com',
    ]
);

$db->insert(
    'users',
    [
        'id' => null,
        
        'created_time_utc' => $time->getUtc(),
        'created_time_user' => $time->getClientApi(),
        
        'first_name' => 'Vasya',
        'second_name' => 'Pupkin',
        
        'e_mail' => 'vasya.pupkin@gmail.com',
    ]
);

$db->insert(
    'users',
    [
        'id' => null,
        
        'created_time_utc' => $time->getUtc(),
        'created_time_user' => $time->getClientApi(),
        
        'first_name' => '<marquee behavior="alternate" direction="right">Advertising</marquee>',
        'second_name' => '<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=-0.004017949104309083%2C51.47612752641776%2C0.00030577182769775396%2C51.478569861898606&layer=mapnik"></iframe>',
        
        'e_mail' => 'company_name@domain.company',
    ]
);

$db->insert(
    'users',
    [
        'id' => null,
        
        'created_time_utc' => $time->getUtc(),
        'created_time_user' => $time->getClientApi(),
        
        'first_name' => '<script>alert(\'Hello, World!\')</script>',
        'second_name' => '<h1>Title</h1>',
        
        'e_mail' => 'user-name@domain.name',
    ]
);
```

---

##### Read
```diff
+ Tested
```

```diff
@@ Select | Data. From table @@
@@ Example 1 @@
```
```php
$db = new Database();

$data = $db->select('users');
var_dump($data);
```

```diff
@@ Example 2 @@
```
```php
$db = new Database();

$data = $db->select(
    'users',
    [
        'id',
        
        'first_name',
        'second_name',
        
        'e_mail',
    ]
);
var_dump($data);
```

---

##### Update
```diff
+ Tested
```

```diff
@@ Update | Data. In table @@
```
```php
$db = new Database();
$time = new Time();

$db->update(
    'users',
    [
        'updated_time_utc' => $time->getUtc(),
        'updated_time_user' => $time->getClientApi(),
        
        'first_name' => 'Vasya',
        'second_name' => 'Pupkin',
        
        'e_mail' => 'vasya.pupkin.new@gmail.com',
    ],
    'id', 2
);
```

---

##### Delete
```diff
+ Tested
```

```diff
@@ Drop | Row. In table @@
```
```php
$db = new Database();

$db->dropRow('users', 'id', 1);
```

---

```diff
+ Tested
```

```diff
@@ Drop | Column. In table @@
```
```php
$db = new Database();

$db->dropColumn('users', 'updated_time_utc');
$db->dropColumn('users', 'updated_time_user');
```

---

```diff
+ Tested
```

```diff
@@ Drop | Table @@
```
```php
$db = new Database();

$db->dropTable('users');
```

---

```diff
+ Tested
```

```diff
@@ Drop | Schema (database) @@
```
```php
$db = new Database();

$db->dropSchema('users');
```

---

```diff
+ Tested
```

```diff
@@ Drop | User @@
@@ ! Run as `root` user @@
```
```php
$db = new Database();

$db->dropUser('webinse');
```

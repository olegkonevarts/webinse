<?php

$ds = DIRECTORY_SEPARATOR;

require_once __DIR__ . "{$ds}Errors.php";

Errors::hideAll();

require_once __DIR__ . "$ds.config{$ds}db.php";

class Autoloader
{
    private static $pathToPhpLibrary = '';
    
    public static function start(string $pathToPhpLibrary)
    {
        self::$pathToPhpLibrary = $pathToPhpLibrary;
        
        spl_autoload_register(
            function ($className) {
                global $ds;
                
                $folderPaths = glob(self::$pathToPhpLibrary . "$ds*$ds");
                
                foreach ($folderPaths as $folderPath) {
                    $path = str_replace(
                        ['/', '\\'],
                        $ds,
                        "$folderPath{$className}.php"
                    );
                    
                    if (file_exists($path)) {
                        require_once $path;
                    }
                }
            }
        );
    }
}

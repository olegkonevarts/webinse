<?php

class Errors
{
    private static $functions = [
        'error_reporting($value);',
        'ini_set(\'display_errors\', $value);',
        'ini_set(\'display_startup_errors\', $value);',
    ];
    
    private static function setup(array $values)
    {
        $i = 0;
        $length = count(self::$functions);
        
        foreach ($values as $value) {
            if ($i === $length) {
                break;
            }
            
            if (isset($value)) {
                eval(self::$functions[$i]);
                $i++;
            }
        }
    }
    
    public static function displayAll()
    {
        self::setup([E_ALL, 1, 1]);
    }
    
    public static function hideAll()
    {
        self::setup([0, 0, 0]);
    }
}
